function toCamelCase() {
    let str = document.getElementById('text').value.toLowerCase();
    function uppercase(str) {
        return str[1].toUpperCase();
    }
    let strres = str.replace(/_([a-z])/g, uppercase);
    let res = document.getElementById('res');
    res.innerHTML = strres;
}
