function generate() {
    let text = document.getElementById("data").value;
    let regex =/(\w+)\s(\w+)\s(\w+)\s([A-Za-z0-9 ,\n]+)\s(\d{2}-\d{2}-\d{4})\s(\w+)\s([A-Za-z0-9!@#$%^&*()_\-=+\\|]+)\s(\w+@\w+.\w+)/g;
    let res;
    let table = document.getElementById("table");
    if (table.innerHTML === "") {
        table.innerHTML += "<tr style='font-weight: bold'>" + "<td>FirstName</td>" + "<td>Surname</td>" +
            "<td>Gender</td>" + "<td>Address</td>" + "<td>Birth date</td>" + "<td>Nickname</td>" +
            "<td>Password</td>" + "<td>E-mail</td>" + "</tr>";
    }
    while (res = regex.exec(text)) {
        let dataCells = "";
        for (let i = 1; i < res.length; i++) {
            dataCells += `<td>${res[i]}</td>`;
        }
        table.innerHTML += `<tr>${dataCells}</tr>`;
    }
    let td = document.getElementsByTagName("td");
    for (let i = 0; i < td.length; i++)
    {
        td[i].style.border = "1px solid #000";
    }
}
