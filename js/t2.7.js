function toSnakeCase() {
  let str = document.getElementById('text').value;
  function lowercase(match) {
    return '_' + match.toLowerCase();
  }
    let strres = str.replace(/([A-Z])/g, lowercase).toLowerCase();
    let res = document.getElementById('res');
    res.innerHTML = strres;
}
